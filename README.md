# R4AVA07 Python Example

A python snippet that demonstrates RS485 comms between a `USB-RS485-WE` and an `R4AVA07`. Documentation on the `R4AVA07` can be found in the `./assets` directory.

## Getting started

Simply run the following to execute.

```shell
python3 sensor_read_example.py
```