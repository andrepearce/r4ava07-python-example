import serial, time

SERIAL_DEVICE = '/dev/tty.usbserial-FTYJITJ9'
PACKET_DELAY = 0.5

COMMANDS = {
    'read_addr_with_broadcast': b'\xFF\x03\x00\x0E\x00\x01\xF0\x17',
    'read_voltage_ratio_for_ch1': b'\x01\x03\x00\x07\x00\x01\x35\xCB',
    'read_voltage_ratio_for_ch7': b'\x01\x03\x00\x0D\x00\x01\x15\xC9',
    'set_voltage_ratio_for_ch7': b'\x01\x06\x00\x0D\x03\xE8\x18\xB7',
    'read_voltage_value_for_ch1': b'\x01\x03\x00\x00\x00\x01\x84\x0A',
    'read_voltage_value_for_ch7': b'\x01\x03\x00\x06\x00\x01\x64\x0B'
}

with serial.Serial(SERIAL_DEVICE) as ser:

    #######################################
    ##
    ## Get device address using broadcast
    ##
    #######################################
    print("### Get device address ###")
    ser.write(COMMANDS['read_addr_with_broadcast'])
    device_address = ser.read()
    funciton_code = ser.read()
    data_length = ser.read()
    data  = ser.read(2)
    check = ser.read(2)

    print("Device ID: " + str(int.from_bytes(device_address, "big")))
    print("Function Code: " + str(int.from_bytes(funciton_code, "big")))
    print("Data Length: " + str(int.from_bytes(data_length, "big")))
    print("Data: " + str(int.from_bytes(data, "big")))
    print("Check: " + str(int.from_bytes(check, "big")))

    time.sleep(PACKET_DELAY)

    #######################################
    ##
    ## Get voltage ratio of ch1
    ##
    #######################################
    print("### Get voltage ratio of ch1 ###")
    ser.write(COMMANDS['read_voltage_ratio_for_ch1'])
    device_address = ser.read()
    funciton_code = ser.read()
    data_length = ser.read()
    data  = ser.read(2)
    check = ser.read(2)

    print("Device ID: " + str(int.from_bytes(device_address, "big")))
    print("Function Code: " + str(int.from_bytes(funciton_code, "big")))
    print("Data Length: " + str(int.from_bytes(data_length, "big")))
    print("Data: " + str(int.from_bytes(data, "big")))
    print("Check: " + str(int.from_bytes(check, "big")))

    time.sleep(PACKET_DELAY)

    #######################################
    ##
    ## Get voltage value of ch1
    ##
    #######################################
    print("### Get voltage value of ch1 ###")
    ser.write(COMMANDS['read_voltage_value_for_ch1'])
    device_address = ser.read()
    funciton_code = ser.read()
    data_length = ser.read()
    data  = ser.read(2)
    check = ser.read(2)

    print("Device ID: " + str(int.from_bytes(device_address, "big")))
    print("Function Code: " + str(int.from_bytes(funciton_code, "big")))
    print("Data Length: " + str(int.from_bytes(data_length, "big")))
    print("Data: " + str(int.from_bytes(data, "big")) + " -> " + str(int.from_bytes(data, "big")/100) + "v")
    print("Check: " + str(int.from_bytes(check, "big")))

    time.sleep(PACKET_DELAY)

    #######################################
    ##
    ## Get voltage ratio of ch7
    ##
    #######################################
    print("### Get voltage ratio of ch7 ###")
    ser.write(COMMANDS['read_voltage_ratio_for_ch7'])
    device_address = ser.read()
    funciton_code = ser.read()
    data_length = ser.read()
    data  = ser.read(2)
    check = ser.read(2)

    print("Device ID: " + str(int.from_bytes(device_address, "big")))
    print("Function Code: " + str(int.from_bytes(funciton_code, "big")))
    print("Data Length: " + str(int.from_bytes(data_length, "big")))
    print("Data: " + str(int.from_bytes(data, "big")))
    print("Check: " + str(int.from_bytes(check, "big")))

    time.sleep(PACKET_DELAY)

    #######################################
    ##
    ## Set voltage ratio of ch7
    ##
    #######################################
    print("### Set voltage ratio of ch7 ###")
    ser.write(COMMANDS['set_voltage_ratio_for_ch7'])
    device_address = ser.read()
    funciton_code = ser.read()
    reg_addr = ser.read(2)
    reg_value  = ser.read(2)
    check = ser.read(2)

    print("Device ID: " + str(int.from_bytes(device_address, "big")))
    print("Function Code: " + str(int.from_bytes(funciton_code, "big")))
    print("Register Address: " + str(int.from_bytes(reg_addr, "big")))
    print("Register Value: " + str(int.from_bytes(reg_value, "big")))
    print("Check: " + str(int.from_bytes(check, "big")))

    time.sleep(PACKET_DELAY)

    #######################################
    ##
    ## Get voltage value of ch7
    ##
    #######################################
    print("### Get voltage value of ch7 ###")
    ser.write(COMMANDS['read_voltage_value_for_ch7'])
    device_address = ser.read()
    funciton_code = ser.read()
    data_length = ser.read()
    data  = ser.read(2)
    check = ser.read(2)

    print("Device ID: " + str(int.from_bytes(device_address, "big")))
    print("Function Code: " + str(int.from_bytes(funciton_code, "big")))
    print("Data Length: " + str(int.from_bytes(data_length, "big")))
    print("Data: " + str(int.from_bytes(data, "big")) + " -> " + str(int.from_bytes(data, "big")/100) + "v")
    print("Check: " + str(int.from_bytes(check, "big")))
